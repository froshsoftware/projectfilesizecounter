// === CONFIGURATION ===

GROUP_NAME = 'ZAEHLEN';
COUNT_FLAG = 'ZAEHLEN';
COUNTED_FLAG = 'ZAEHLEN';
CSV_TMP_FOLDER = 'C:\\Users\\Administrator\\Desktop\\csvTemp';

CSV_ELO_FOLDER = '¶Projektauswertungen';
CSV_ELO_MASK = 0;
ELO_REMINDER_NAME = 'Auswertung';

// === CONFIGURATION END ===

(function(){
  // 1. find all folder with the marker
  var folders = findAllMarkedFolders();

  if(folders.length < 1) {
    log.info('no folders marked for export');
    return;
  }

  var csv = '';
  csv += formatCsvHeader();
  for(var i in folders) {
    var folder = folders[i];

    forEachChild(folder, function(child){
      var isDoc = child.type >= SordC.LBT_DOCUMENT && child.type <= SordC.LBT_DOCUMENT_MAX;
      if(!isDoc){
        var folderInfo = calcFolderSize(child);
        csv += formatCsvRow(folderInfo);
      }
    });
  }

  var objId = archiveCsv(csv);

// mark as counted
  for(var i in folders) {
    var folder = folders[i];

    for(var k in folder.objKeys) {
      var line = folder.objKeys[k];
      if(line.name == GROUP_NAME) {
        line.data = [ COUNTED_FLAG ];
        break;
      }
    }

    ixConnect.ix().checkinSord(folder, new SordZ(SordC.mbObjKeys), LockC.NO)
  }

  setNotification(objId);
})();

// === FUNCTIONS ===

function calcFolderSize(folder) {
  var folderInfo = {
    folder: folder,
    documentsCount: 0,
    foldersCount: 0,
    documentsFileSize: 0,
    versionsCount: 0,
    versionsFileSize: 0
  };
  forEachChild(folder, function(child){
    if(child.isDeleted()) {
      log.warn('deleted document: '+child.name);
      return;
    }

    var isDoc = child.type >= SordC.LBT_DOCUMENT && child.type <= SordC.LBT_DOCUMENT_MAX;
    if(isDoc) {
      try {
        if (child.docVersion.size == null){
          log.warn('document without size: '+child.name);
          return;
        }
        folderInfo.documentsCount++;
        folderInfo.documentsFileSize += child.docVersion.size;

        var editInfo = ixConnect.ix().checkoutDoc(child.id + '', '-1', EditInfoC.mbSordDoc, LockC.NO);
        var docVersions = editInfo.document.docs;
        for (var i in docVersions) {
          var docVersion = docVersions[i];
          if(docVersion.isWorkVersion() || docVersion.isDeleted()) {
            log.warn('document version thats either work version or deleted for sord: '+child.name);
            continue;
          }

          var size = docVersion.size;

          folderInfo.versionsCount++;
          folderInfo.versionsFileSize += size;
        }
      } catch(e) {
        log.warn("Inkonsistentes Dokument: " + child.name + '| Fehler: '+e);
      }
    } else {
      folderInfo.foldersCount++;
    }
  });
  return folderInfo;
}

function findAllMarkedFolders() {
  var findInfo = new FindInfo();
  var findByIndex = new FindByIndex();

  findInfo.findOptions = new FindOptions();
  findInfo.findOptions.inclDeleted = false;

  var objKey = new ObjKey();
  var keyData = [ COUNT_FLAG ];
  objKey.name = GROUP_NAME;
  objKey.data = keyData;

  var objKeys = [ objKey ];

  findByIndex.objKeys = objKeys;
  findInfo.findByIndex = findByIndex;

  var findResult = ixConnect.ix().findFirstSords(findInfo, 1000, SordC.mbAll);
  var sords = findResult.sords;
  ixConnect.ix().findClose(findResult.searchId);
  return sords;
}

function forEachChild(sord, callback, isChild) {
  if(!isChild) {
    callback(sord);
  }
  var childs = ix.collectChildren(sord.id, /* with refs */ false);
  for(var i in childs) {
    var child = childs[i];
    callback(child);

    forEachChild(child, callback, true)
  }
}

function formatCsvHeader() {
  return 'Ordner_Name;'+
    'Anzahl_Dokumente;'+
    'Anzahl_Ordner;'+
    'Groesse_Dokumente_in_GB;'+
    'Anzahl_alte_Versionen;'+
    'Groesse_alte_Versionen_in_GB;'+
    'Groesse_Gesamt_in_GB;'+
    '\r\n';
}

function formatCsvRow(folderInfo) {
  var documentsSizeInGb = folderInfo.documentsFileSize
    / 1024 // KB
    / 1024 // MB
    / 1024; // GB

  var versionsSizeInGb = folderInfo.versionsFileSize
    / 1024 // KB
    / 1024 // MB
    / 1024; // GB

  var allSizeInGb = documentsSizeInGb + versionsSizeInGb;

  var fullPath = getFullEloPath(folderInfo.folder);
  return fullPath +';' +
    folderInfo.documentsCount +';' +
    folderInfo.foldersCount +';' +
    formatNumberGER(documentsSizeInGb) +';' +
    folderInfo.versionsCount +';' +
    formatNumberGER(versionsSizeInGb) +';' +
    formatNumberGER(allSizeInGb) + ';' +
    '\r\n';
}

function writeCsvToTemp(csvData) {
  var timeStamp = new Date();
  var name = ELO_REMINDER_NAME + '_' + elo.timeStamp(timeStamp);
  var fullName = CSV_TMP_FOLDER + '\\' + name + '.csv';
  var writer;
  try {
    writer = new java.io.PrintWriter(fullName, 'UTF-8');
    writer.println(csvData)
  }
  finally {
    if(writer) writer.close()
  }

  return {
    fullName: fullName,
    name: name
  }
}

function archiveCsv(csv) {
  var file = writeCsvToTemp(csv);

  var parent = elo.preparePath(CSV_ELO_FOLDER);
  var ed = ixConnect.ix().createDoc(parent, CSV_ELO_MASK, null, EditInfoC.mbOnlyId);
  ed.sord.name = file.name;
  ed.document = new Document();
  ed.document.docs = [ new DocVersion() ];
  ed.document.docs[0].pathId = ed.sord.path;
  ed.document.docs[0].encryptionSet = ed.sord.details.encryptionSet;

  var jFile = new java.io.File(file.fullName);
  ed.document = ixConnect.ix().checkinDocBegin(ed.document);
  ed.document.docs[0].uploadResult = ixConnect.upload(ed.document.docs[0].url, jFile);
  ed.document = ixConnect.ix().checkinDocEnd(ed.sord, SordC.mbAll, ed.document, LockC.NO);

  jFile['delete']();

  return ed.document.objId
}

function setNotification(objId) {
  var reminder = ixConnect.ix().createReminder(objId);
  reminder.name = ELO_REMINDER_NAME;
  reminder.receiverId = -1;
  reminder.receiverName = null;

  ixConnect.ix().checkinReminder(reminder, [ EM_USERID ], true, LockC.NO)
}

function getFullEloPath(sord) {
  var fullPath = '';

  var parent = sord;
  do {
    fullPath = '\u00B6'+parent.name + fullPath;
    parent = ixConnect.ix().checkoutSord(parent.parentId, EditInfoC.mbAll, LockC.NO).sord;
  } while(parent.id > 1);

  return fullPath;
}

function formatNumberGER(num) {
  return (num + '').replace('.', ',');
}