importPackage(Packages.de.elo.ix.client);

// === CONFIGURATION ===

ELO_AS_SCRIPT_NAME = 'test'

// === CONFIGURATION END ===

function getScriptButton506Name() {
  return "Projekte auswerten";
}

function getScriptButtonPositions() {
  return "506,archive,information";
}

function eloScriptButton506Start(){
  var ixCon = ixc;

  var serverInfo = ixCon.getServerInfo()
  var ixServer = serverInfo.indexServers[0];
  var url = ixServer.url;

  workspace.setFeedbackMessage(url)

  var ixUrlRegex = /https?:\/\/(.+):(\d+)\/ix-(.+)\/ix/
  var match = url.match(ixUrlRegex)

  var server = match[1]
  var port = match[2]
  var archive = match[3]

  workspace.getELOas(
    /* server name */ server,
    /* server port */ port,
    /* service */ 'as-'+archive,
    /* with ticket */ true,
    /* ruleset name */ ELO_AS_SCRIPT_NAME,
    /* param 1 */ '',
    /* param 2 */ '',
    /* param 3 */ ''
  )

  workspace.setFeedbackMessage('Auswertung gestartet ...')
}
